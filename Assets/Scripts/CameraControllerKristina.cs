﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CameraController : MonoBehaviour
{
    public float speedH = 2.0f;
    public float speedV = 2.0f;

    private float yaw = 0.0f;
    private float pitch = 0.0f;

    public float speed = 10.0f;
    private Rigidbody rb;
   
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        yaw += speedH * Input.GetAxis("Mouse X");
        pitch -= speedV * Input.GetAxis("Mouse Y");
        //rotacija misem
        transform.eulerAngles = new Vector3(pitch, yaw, 0.0f);
      
        float moveHorizontal = Input.GetAxisRaw("Horizontal");
        float moveVertical = Input.GetAxisRaw("Vertical");
        float moveUp = Input.GetAxisRaw("Jump");

        Vector3 movement = new Vector3(moveHorizontal, moveUp, moveVertical);
      
        rb.AddForce(movement * speed);
    }

}


